.. _how_to_get_started:

*****************************************
Get started
*****************************************

Prerequisites
====================

Binary dependencies
--------------------

* ``make``
* ``python >= 3.7.3``
* ``virtualenv``
* ``pip3``
* `direnv <https://direnv.net/docs/installation.html>`_ 

Configuration
--------------------

* direnv `hooked to your shell <https://direnv.net/docs/hook.html>`_

Setup workspace
====================

* Run:

.. code:: bash

    git clone https://gitlab.com/tuuux/galaxie-clans.git
    cd galaxie-clans/
    direnv allow
    make env

.. admonition:: CONGRATULATIONS
    :class: important

    You are ready to work with the project.
