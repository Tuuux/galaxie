::

  k3s_server_options: "--disable traefik --write-kubeconfig /root/.kube/config"
  k3s_helm_version: "3.5.3"

  k3s_ingress_nginx_chart_repository_name: "ingress-nginx"
  k3s_ingress_nginx_chart_repository_url: "https://kubernetes.github.io/ingress-nginx"
  k3s_ingress_nginx_chart_version: "3.24.0"
  k3s_ingress_nginx_chart_name: "ingress-nginx"
  k3s_ingress_nginx_chart_values_file: /var/lib/k3s/ingress-nginx.values.yml

  k3s_cert_manager_chart_repository_name: "jetstack"
  k3s_cert_manager_chart_repository_url: "https://charts.jetstack.io"
  k3s_cert_manager_chart_version: "1.2.0"
  k3s_cert_manager_chart_name: "cert-manager"
  k3s_cert_manager_chart_values_file: /var/lib/k3s/cert-manager.values.yml
